# nodejs-app-ci-pipeline Project 2024


#### Project Outline

Our team members want to collaborate on your NodeJS application, where you list developers with their projects. So they ask us to set up a git repository for it.
Also, we think it's a good idea to add tests, to test that no one accidentally breaks the existing code.
Moreover, you all decide every change should be immediately built and pushed to the Docker repository, so everyone can access it right away.
For that they ask us to set up a continuous integration pipeline.

#### Project Code for main and shared library branch

[Link to Master](https://gitlab.com/FM1995/nodejs-app-ci-pipeline/-/blob/master)

[Link to shared library](https://gitlab.com/FM1995/nodejs-app-ci-pipeline/-/blob/jenkins-shared-library)

[Link to shared library contents](https://gitlab.com/FM1995/second-jenkins-shared-library)


## Table of Contents
- [Dockerize your NodeJS App](#dockerize-your-nodejs-app)
- [Create a full pipeline for NodeJS App](#create-a-full-pipeline-for-nodejs-app)
- [Manually deploy new Docker Image on server](#manually-deploy-new-docker-image-on-server)
- [Bonus! Let’s automate the script to deploy to the server using the docker commands](#bonus-lets-automate-the-script-to-deploy-to-the-server-using-the-docker-commands)
- [Extract into Jenkins Shared Library](#extract-into-jenkins-shared-library)




## Dockerize your NodeJS App

Lets Dockerize our application

```
touch Dockerfile
```

![Image 1](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image1.png)


And can configure the below

1.	FROM node:latest: Specifies the base image as the latest version of the official Node.js runtime image from the Docker Hub.

2.	WORKDIR /app: Sets the working directory inside the container as /app, which will be the main location for the application files.

3.  COPY . . : Copies the entire project directory, including all files and subdirectories, from the local machine to the working directory in the container. This ensures that all project files are available inside the container for running the Node.js application.


4.	EXPOSE 3000: Exposes port 3000 from the container to the outside world. This informs Docker that the Node.js application running inside the container will be listening on port 3000.

5.	CMD ["node", "app/server.js"]: Specifies the command to run the Node.js application when the container starts. It executes the node app/server.js command, launching the Node.js application with the server.js file as the entry point.

![Image 2](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image2.png)

Now ready to push nodejs project with dockerfile to git

![Image 3](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image3.png)

![Image 4](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image4.png)


## Create a full pipeline for NodeJS App

We want the following steps to be included in your pipeline:

Increment version
The application's version and docker image version should be incremented.

Run tests

You want to test the code, to be sure to deploy only working code. When tests fail, the pipeline should abort.

Build docker image with incremented version

Push to Docker repository

Commit to Git

The application version increment must be committed and pushed to a remote Git repository.

Lets first create the docker credentials

![Image 5](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image5.png)

Configure Node Tool in Jenkins Configuration

As I am have already installed Node and installed the plugin 

![Image 6](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image6.png)

Can proceed to Manage Jenkins -> Tools and add the node version

![Image 7](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image7.png)

And the name ‘node’ is what we will be using to reference it in the Jenkinsfile
Next step is to add the JenkinsFile and push it to the repo

![Image 8](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image8.png)

![Image 9](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image9.png)

Next step is to configure the multibranch pipeline and branch sources to have access to the git repo with the Jenkins file

![Image 10](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image10.png)

![Image 11](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image11.png)

Now ready

![Image 12](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image12.png)

Also need to install plugin Pipeline Utility Steps, This contains readJSON function, that we will use to read the version from package.json

![Image 13](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image13.png)

![Image 14](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image14.png)


Ready to create the Jenkins file to achieve the above

Let’s take a basic Jenkins file and convert it

![Image 15](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image15.png)

1.	Increment Version:
•	You need to update the application's version and the Docker image version.
•	The version update should be committed and pushed to the remote Git repository.
•	You can use tools like npm version or other version management mechanisms to increment the version in your project's package.json file.

2.	Run Tests:
•	You want to execute the tests to ensure the code is functioning correctly.
•	The pipeline should abort if any of the tests fail, indicating that there's an issue in the code.

3.	Build Docker Image with Incremented Version:
•	After updating the version, you need to build a Docker image of your NodeJS application.
•	The Docker image should include the incremented version so that you can track changes easily.

4.	Push Docker Image to Docker Repository:
•	Once the Docker image is built, you should push it to a Docker repository (e.g., Docker Hub) so that it can be accessed and deployed.

5.	Commit Version Update to Git:
•	The application version increment must be committed and pushed back to the remote Git repository.
•	This ensures that the version changes are reflected in the source code repository.

Lets increment the version 

This pipeline is responsible for incrementing the version in the package.json file, reading the updated version, and constructing an IMAGE_NAME with the updated version and the Jenkins BUILD_NUMBER. This IMAGE_NAME can then be used for tasks such as building and tagging Docker images or any other deployment-related operations. Note that this script is only handling the "Increment version" stage, and you may need to add more stages for tasks like running tests, building Docker images, pushing to Docker registry, and committing the version changes to Git.

![Image 16](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image16.png)

Lets then run the tests

The "Run tests" stage in the Jenkins pipeline script is responsible for running tests on your Node.js application. It performs the following actions:

It changes the working directory to the "app" directory using the dir block. This is necessary to ensure that the tests are executed in the correct context, as the tests are typically located within the "app" directory.

It then runs the command npm install, which installs all the application's dependencies needed for running tests. This is a common step before running tests to ensure that the test environment has all the required dependencies.

After installing the dependencies, the script proceeds to run the tests using the command npm test. The specific test command can vary depending on how your Node.js application is configured and which testing framework you are using. 

![Image 17](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image17.png)

Next step, build and push image

The withCredentials block securely handles Docker credentials using the usernamePassword method. The credentials with the ID "Fuad95" are defined in the Jenkins credentials setup, and their username and password are set to the environment variables USER and PWD, respectively.

The docker build command is used to build the Docker image with the -t flag specifying the image name as fuad95/nodejs-app with the version tag ${env.Image_Name}. The . at the end of the command indicates that the Dockerfile is located in the current directory.

The docker login command is used to authenticate with the Docker registry. The -u flag is used to specify the username, and the --password-stdin flag is used to read the password from the standard input. The password is passed in using the ${PWD} variable, which contains the current working directory.

Finally, the docker push command is used to push the Docker image to the Docker registry with the same image name and version tag as before.


![Image 18](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image18.png)


Next we will Commit Version Update to Git

Overall, this stage is responsible for configuring Git credentials, committing and pushing the version bump changes to the remote repository, ensuring that the version increment is recorded in the codebase.

![Image 19](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image19.png)

Jenkinsfile finalised and pushed to git repo

![Image 20](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image20.png)

New changes due to build errors

New Dockerfile

![Image 21](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image21.png)

With this Dockerfile placed in the root of your project, Jenkins will be able to find the package*.json files and proceed with the build without the COPY error.

![Image 22](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image22.png)

Utilising the new updates in the plugins, utilising credentialsId instead of credentialsID. 

Also instead of 

![Image 23](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image23.png)

Was getting the below error

![Image 24](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image24.png)

Re-worked the code

![Image 25](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image25.png)

Finalised code

![Image 26](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image26.png)

Ready for build

![Image 27](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image27.png)

Looking at the logs

In summary, the logs indicate that the Jenkins pipeline has been executed successfully. The pipeline went through multiple stages, including fetching code from Git, incrementing the version, running tests, building and pushing a Docker image, and finally committing and pushing the changes back to the Git repository. The pipeline completed with a "SUCCESS" status, which means all stages were executed without any errors.

![Image 28](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image28.png)

![Image 29](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image29.png)

![Image 30](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image30.png)

Checking the Docker repo

![Image 31](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image31.png)

Can see it has been pushed to Docker along with the build number

Going in to the Jenkins-jobs branch the below

![Image 32](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image32.png)

![Image 33](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image33.png)

![Image 34](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image34.png)

## Manually deploy new Docker Image on server

After the pipeline has run successfully we want to manually deploy the new docker image on the droplet server.

First step
Login to docker in the server

```
docker login -u fuad95 -p XXXXX
```

![Image 35](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image35.png)

Based on the docker image

![Image 36](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image36.png)

```
docker pull fuad95/nodejs-app:1.1.0.29
```

![Image 37](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image37.png)

Can then use docker run command to start the container with the image

```
docker run -d --name CICD-Deployment -p 3000:3000 fuad95/nodejs-app:1.1.0-29
```

![Image 38](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image38.png)


Open port 3000 on the droplet

![Image 39](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image39.png)

Can now access on the URL

```
http://64.227.44.149:3000/
```

![Image 40](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image40.png)

## Bonus! Let’s automate the script to deploy to the server using the docker commands

Lets begin by adding the credentials to Jenkins, 'server-credentials'

![Image 41](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image41.png)

![Image 42](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image42.png)

And add the plugin 'Publish Over SSH'

![Image 43](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image43.png)

The main thing for this to work is the ssh key

First go in to the docker container running the Jenkins server

```
docker exec -it f8e547093652 bash
```

![Image 50](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image50.png)

Then generate the key

```
ssh-keygen -t rsa -N "" -f /var/jenkins_home/.ssh/id_rsa
```

![Image 51](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image51.png)

Can then copy it to my server

![Image 52](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image52.png)

![Image 53](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image53.png)

Can then add key in the created ssh credential in jenkins

![Image 67](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image67.png)

Finally make changes to jenkinsfile script and adding the 'deploy stage'

1.	withCredentials: Similar to the previous stage, this block allows accessing sensitive information securely from Jenkins credentials. In this case, it retrieves an SSH private key (credentials ID: 'server-credentials') and assigns it to the variable ${SSH_KEY}. Additionally, it sets the username for SSH authentication as 'root'.

2.	def remoteServer = '64.227.44.149': Sets the IP address or hostname of the remote server where the Docker image will be deployed.

3.	def remoteDir = '/path/to/remote/directory': Specifies the remote directory where the Docker image files might be copied (not used in this code snippet).

4.	 Connects to the remote server using SSH with the specified private key ${SSH_KEY} and username (root). Then it pulls the latest version of the Docker image from the Docker repository. The ${env.Image_Name} variable is used here to specify the specific image version to pull.

```
sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY} root@${remoteServer} 'docker pull fuad95/nodejs-app:$env.Image_Name'"
```

5.	Stops the existing Docker container named 'CICD-Deployment' on the remote server, if it exists. The || true part ensures that the command doesn't throw an error if the container doesn't exist.

```
sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY} root@${remoteServer} 'docker stop CICD-Deployment || true'"
```


6.	Removes the existing Docker container named 'CICD-Deployment' on the remote server, if it exists. The || true part ensures that the command doesn't throw an error if the container doesn't exist.

```
sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY} root@${remoteServer} 'docker rm CICD-Deployment || true'"
```


7.	Runs a new Docker container on the remote server. It uses the previously pulled image tagged with the version specified by ${env.Image_Name}. The container is named 'CICD-Deployment', and it maps port 3000 on the remote server to port 3000 in the container (-p 3000:3000). The container runs in detached mode (-d).

```
sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY} root@${remoteServer} 'docker run -d --name CICD-Deployment -p 3000:3000 fuad95/nodejs-app:$env.Image_Name'"
```


![Image 55](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image55.png)

![Image 56](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image56.png)

Finalized Jenkinsfile

![Image 57](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image57.png)

Now ready for build and can access via browser

![Image 45](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image45.png)

![Image 46](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image46.png)

Checking the docker processes and the image in the Docker repository

![Image 48](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image48.png)

![Image 49](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image49.png)

And can now access on droplet-ip and port of the container

![Image 47](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image47.png)


## Extract into Jenkins Shared Library

A colleague from another project tells us, they are building a similar Jenkins pipeline and they could use some of your logic. So you suggest creating a Jenkins Shared Library to make your Jenkinsfile code reusable and shareable.

Therefore, we do the following:

Extract all logic into Jenkins-shared-library with parameters and reference it in Jenkinsfile.

Steps

Create a new Jenkins-shared-library in git 

[Link to shared library contents](https://gitlab.com/FM1995/second-jenkins-shared-library)

![Image 58](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image58.png)

Can then configure the ‘Global Pipeline Libraries’

![Image 59](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image59.png)

Also create a new pipeline project

![Image 60](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image60.png)

Lets also create the vars files for the Jenkins shared library 

![Image 61](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image61.png)

Re-define the Jenkins file to call on the functions

![Image 62](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image62.png)

Now ready for build

And build is a success

![Image 63](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image63.png)

Lets also see the docker repo

Can see it is in sync with the build number and patch number

![Image 64](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image64.png)

Version bump still works

![Image 65](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image65.png)

![Image 66](https://gitlab.com/FM1995/nodejs-app-ci-pipeline-project-2024/-/raw/main/Images/Image66.png)
























